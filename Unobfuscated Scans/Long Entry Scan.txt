# Parameters
input LookbackPeriod = 44;  
input SmoothingPeriod = 44;  
input StdDeviationsMultiple = 1.5;  
input SlowTrendPeriod = 50;  
input FastTrendPeriod = 10;  
input RequireVolume = no;  
input VolumeMAPeriod = 64;  
input VolumeMultiple = 2.0;  
input Target1RetracementLevel = 38.0;  
input Target2RetracementLevel = 62.0;  
input TrailingStopRetracementLevel = 62.0;  
input TradingStyle = {default LONG_AND_SHORT, LONG_ONLY, SHORT_ONLY };   

input ScanLookbackPeriod = 1;
# Do not modify the code below


 







input ColorBars = yes;
input ShowIET = yes;
input IETAutoLookback = yes;
input IETStartDate = 20210101;



def LongSLDist;
def ShortSLDist;
def lep;
def sep;
def currentlyLong;
def currentlyShort;
def pt1hit;
def pt2hit;
def slhit;


pt1hit = if IsNaN(pt1hit[1]) then no else pt1hit[1];
pt2hit = if IsNaN(pt2hit[1]) then no else pt2hit[1];
slhit = if IsNaN(slhit[1]) then no else slhit[1];


def highestHigh = Highest(high, LookbackPeriod);
def lowestLow = Lowest(low, LookbackPeriod);

def candleHeight = high - low;
def zoneHeight = highestHigh - lowestLow;
def zoneArea = zoneHeight * LookbackPeriod;
def candleHeightSum = Sum(candleHeight, LookbackPeriod);
def ratio =  candleHeightSum / zoneArea;
def ratio_1 = ratio[1];
def zoneArea_1 = zoneArea[1];
def zoneRatio = zoneArea / zoneArea_1 - 1;
def sd = StDev(ratio, SmoothingPeriod);
def expansionNorm = MovingAverage(AverageType.SIMPLE, ratio, SmoothingPeriod);
def zoneRatio_1 = zoneRatio[1];

def upperBand = expansionNorm + sd * StdDeviationsMultiple;
def lowerBand = expansionNorm - sd * StdDeviationsMultiple;
def expansionNormRatio =   (zoneRatio - lowerBand) / (upperBand - lowerBand);

def Expansion = 1.0 - ratio;
def Mean = 1.0 - expansionNorm;
def TopBand = 1.0 - lowerBand;
def BottomBand = 1.0 - upperBand;

AssignPriceColor(if ColorBars then if zoneRatio > 0 then if zoneRatio >= zoneRatio_1 then GlobalColor("Increasing Expansion Color") else GlobalColor("Expansion Color") else if zoneRatio <= zoneRatio_1 then GlobalColor("Increasing Compression Color") else GlobalColor("Compression Color") else Color.CURRENT);

def fastMA = MovingAverage(AverageType.SIMPLE, close, FastTrendPeriod);
def slowMA = MovingAverage(AverageType.SIMPLE, close, SlowTrendPeriod);

def longPriceBreakout = fastMA > slowMA and high > highestHigh[1];
def shortPriceBreakdown = fastMA < slowMA and low < lowestLow[1];

def expansionBreakout = Expansion >= TopBand and Expansion[1] < TopBand[1];
def volumeSatisfied = (! RequireVolume) or (volume > VolumeMultiple * MovingAverage(AverageType.SIMPLE, volume, VolumeMAPeriod));

def validLongBreakout = longPriceBreakout and expansionBreakout and volumeSatisfied;
def validShortBreakdown = shortPriceBreakdown and expansionBreakout and volumeSatisfied;

def canTakeLongs = TradingStyle == TradingStyle.LONG_AND_SHORT or TradingStyle == TradingStyle.LONG_ONLY;
def canTakeShorts = TradingStyle == TradingStyle.LONG_AND_SHORT or TradingStyle == TradingStyle.SHORT_ONLY;

def LongEntry = if validLongBreakout and canTakeLongs and !currentlyLong[1] then low else Double.NaN;
plot LongEntryScan = !IsNaN(LongEntry) within ScanLookbackPeriod bars;
def ShortEntry = if validShortBreakdown and canTakeShorts and !currentlyShort[1] then high else Double.NaN;

def LongPT1Hit;
def LongPT2Hit;
def LongSLHit;
def LongPT1Var;
def LongPT2Var;
def LongSLVar;

LongSLDist = if (!IsNaN(LongEntry) or !IsNaN(ShortEntry)) then highestHigh - lowestLow else  if IsNaN(LongSLDist[1]) then Double.NaN else LongSLDist[1];

currentlyLong = if (!IsNaN(LongEntry)) then yes else if (!IsNaN(ShortEntry)) then no else if (IsNaN(LongPT2Hit[1]) or IsNaN(LongSLHit[1])) then no else currentlyLong[1];

lep = if (!IsNaN(LongEntry)) then close else if currentlyLong then lep[1] else Double.NaN;

LongSLVar = if !IsNaN(LongEntry) then close - LongSLDist * TrailingStopRetracementLevel / 100.0 else if IsNaN(LongSLVar[1]) then Double.NaN else if (currentlyLong and !LongSLHit[1] and !LongPT2Hit[1]) then (if close[1] - LongSLDist[1] * TrailingStopRetracementLevel / 100.0 > LongSLVar[1] then close[1] - LongSLDist[1]* TrailingStopRetracementLevel / 100.0 else LongSLVar[1]) else Double.NaN;
def LongSL = LongSLVar;
LongSLHit = IsNaN(LongEntry) and currentlyLong and low <= LongSLVar;

LongPT2Var = if !IsNaN(LongEntry) then close + LongSLDist * Target2RetracementLevel / 100.0 else if IsNaN(LongPT2Var[1]) then Double.NaN else if (currentlyLong and !LongPT2Hit[1] and !LongSLHit) then LongPT2Var[1] else Double.NaN;
def LongPT2 = LongPT2Var;
LongPT2Hit = currentlyLong and high >= LongPT2Var;

LongPT1Var = if !IsNaN(LongEntry) then close + LongSLDist * Target1RetracementLevel / 100.0 else if IsNaN(LongPT1Var[1]) then Double.NaN else if (currentlyLong and !LongPT1Hit[1] and !LongSLHit) then LongPT1Var[1] else Double.NaN;
def LongPT1 = LongPT1Var;
LongPT1Hit = currentlyLong and high >= LongPT1Var;

def ShortPT1Hit;
def ShortPT2Hit;
def ShortSLHit;
def ShortPT1Var;
def ShortPT2Var;
def ShortSLVar;

ShortSLDist = if (!IsNaN(LongEntry) or !IsNaN(ShortEntry)) then highestHigh - lowestLow else if IsNaN(ShortSLDist[1]) then Double.NaN else ShortSLDist[1];

currentlyShort = if (!IsNaN(ShortEntry)) then yes else if (!IsNaN(LongEntry)) then no else if (IsNaN(ShortPT2Hit[1]) or IsNaN(ShortSLHit[1])) then no else currentlyShort[1];

sep = if (!IsNaN(ShortEntry)) then close else if currentlyShort then sep[1] else Double.NaN;

ShortSLVar = if !IsNaN(ShortEntry) then close + ShortSLDist * TrailingStopRetracementLevel / 100.0 else if IsNaN(ShortSLVar[1]) then Double.NaN else if (currentlyShort and !ShortSLHit[1] and !ShortPT2Hit[1]) then (if close[1] + ShortSLDist[1] * TrailingStopRetracementLevel / 100.0 < ShortSLVar[1] then close[1] + ShortSLDist[1]* TrailingStopRetracementLevel / 100.0 else ShortSLVar[1]) else Double.NaN;
def ShortSL = ShortSLVar;
ShortSLHit = IsNaN(ShortEntry) and currentlyShort and high >= ShortSLVar;

ShortPT2Var = if !IsNaN(ShortEntry) then close - ShortSLDist * Target2RetracementLevel / 100.0 else if IsNaN(ShortPT2Var[1]) then Double.NaN else if (currentlyShort and !ShortPT2Hit[1] and !ShortSLHit) then ShortPT2Var[1] else Double.NaN;
def ShortPT2 = ShortPT2Var;
ShortPT2Hit = currentlyShort and low <= ShortPT2Var;

ShortPT1Var = if !IsNaN(ShortEntry) then close - ShortSLDist * Target1RetracementLevel / 100.0 else if IsNaN(ShortPT1Var[1]) then Double.NaN else if (currentlyShort and !ShortPT1Hit[1] and !ShortSLHit) then ShortPT1Var[1] else Double.NaN;
def ShortPT1 = ShortPT1Var;
ShortPT1Hit = currentlyShort and low <= ShortPT1Var;
